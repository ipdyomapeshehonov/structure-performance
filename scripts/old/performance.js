JIRA_HOST = 'performance.in.almworks.com';

jira = {
  url: 'http://' + JIRA_HOST,
  logPath: [{
    remote: '/opt/jira/logs',
    id: 'tomcatLogs'
  }, {
    remote: '/opt/jira-home/log',
    id: 'jiraLogs'
  }],
  admin:  {username: 'admin', password:'admin'},
  credentials: function (i) {
    return {
      username: "user" + (i + 1),
      password: "user" + (i + 1)
    }
  }
};

yourkit = yourkit || {};
yourkit.host = 'localhost'; yourkit.port = 10002; // ssh -L 10002:localhost:10001 almdev@performance.in.almworks.com
yourkit.keepRemote = false;

ssh = {
  user: 'almdev',
  host: JIRA_HOST
};
