jira = {
  url: 'http://colors-structure-test.docker1.almworks.com/',
  admin:  {username: 'admin', password:'admin'},
  credentials: function (i) {
    return {
      username: "perfuser" + i,
      password: "password"
    }
  }
};

scenarioParams = {
  structureId: 2,
  poll: { count: 0, pauseSec: 1},
  userCount: 5,
  rampPercent: 10,
  threadCount: 5
};

expectedTiming = {
  testCaches: {
    reset: {
      login: { ignore: true },
      loadRows: {min: 500, max: 1000}
    },
    userCaches: {
      login: { ignore: true },
      loadRows: {min: 20, max: 40}
    },
    consequent: {
      login: { ignore: true },
      loadRows: { min: 4, max: 8}
    }
  }
};