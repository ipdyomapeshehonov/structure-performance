JIRA_HOST = '192.168.21.128';

jira = {
  url: 'http://' + JIRA_HOST + ':8080',
  logPath: [{
    remote: '/opt/atlassian/jira/logs',
    id: 'tomcatLogs'
  }, {
    remote: '/var/atlassian/application-data/jira/log',
    id: 'jiraLogs'
  }],
  admin:  {username: 'admin', password:'admin'},
  credentials: function (i) {
    return {
      username: "perfuser" + i,
      password: "password"
    }
  }
};

yourkit.host = JIRA_HOST;
yourkit.port = 10001;
yourkit.keepRemote = false;

ssh = {
  user: 'dyoma',
  host: JIRA_HOST
};
