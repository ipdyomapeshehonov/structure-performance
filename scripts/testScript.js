/**
 * @typedef {{advanceGeneration: function(string), capturePerformanceSnapshot: function()}} YourKit
 * @typedef {{username: string, password: string}} Environment_Credentials
 * @typedef {{url: string, admin: Environment_Credentials, credentials: function(number)}} Environment_Jira
 * @typedef {{jira: Environment_Jira}} Environment_Cfg
 * @typedef {{yourkit: YourKit, execute: function(Scenario), installPlugin: function(string), logInfo: function(string), logMeta: function(string, string)}} Environment
 * @property {Environment_Cfg} environment
 */
environment;
/** @property {Array.<function(Environment)>} tests */
tests;

environment.jira = {
  url: 'http://colors-structure-test.docker1.almworks.com/',
  admin:  {username: 'admin', password:'admin'},
  credentials: function (i) {
    return {
      username: "perfuser" + i,
      password: "password"
    }
  }
};

environment.yourkit.jar = '/opt/yjp/yjp-2016.02/lib/yjp-controller-api-redist.jar';

tests.push(function (env) {
  if (PLUGIN_JAR) env.installPlugin(PLUGIN_JAR);
  env.yourkit.advanceGeneration("start");
  var scenario = new S_CreateStructures();
  scenario.userCount=2;
  scenario.issueCount=2;
  env.execute(scenario);
  env.execute(scenario);
});

/**
 * @param {ReportEnvironment} env
 */
analyse = function (env) {
  function analyseFailedLines() {
    var success = 0;
    var failure = 0;
    env.log.lines.forEach(function (l) {
      if (l.success) success++;
      else failure++;
    });
    env.append(failure == 0,
        "Total lines: " + (success + failure) + ". Failed: " + failure,
        JSON.stringify({success: success, failure: failure})
    )
  }

  /**
   * @param {string} actName
   * @return DiscreetValue
   * */
  function meanResponseTime(actName) {
    var samples = [];
    env.log.activityRequests(actName).forEach(function (r) { samples.push(r.totalDuration.toMillis()) });
    return env.discreetValue(samples);
  }
  function analyzeTimes() {
    var max = {"populateStructure" : 100 };
    var act;
    var success;
    var description;
    var details;
    for (act in max) {
      var timeValue = meanResponseTime(act);
      details = {activity: act, threshold: max[act]};
      if (!timeValue.hasData()) {
        success = false;
        description = "Not logged";
      } else {
        var time = timeValue.meanAndDispersion;
        success = max[act] > time.mean;
        description = time.mean + " (sd=" + time.SD + "). Threshold: " + max[act];
        details.mean = time.mean;
        details.sd = time.SD;
      }
      env.append(success, act + ": " + description, JSON.stringify(details));
    }
  }
  function listActivities() {
    var actNames = [];
    env.log.leafActivities.forEach(function (x) {
      actNames.push(x);
    });
    env.append(true, "Activities: " + actNames.length, JSON.stringify(actNames));
  }
  function randomFailure() {
    var number = Math.random();
    env.append(number < 0.5, "Random failure", JSON.stringify({value: number}));
  }
  listActivities();
  analyseFailedLines();
  analyzeTimes();
};

/**
 * @interface
 * @type {{userCount: number, rampMillis: number, threadCount: number}}
 */
function Scenario() {}
Scenario.init = function (self, javaClass) {
  self.javaClass = javaClass;
  self.userCount = 50;
  self.threadCount = 40;
  self.rampMillis = 0;
};

/**
 * @implements Scenario
 * @type {{singleUser: boolean, issueCount: number, rampMillis: number, threadCount: number}}
 * @constructor
 */
function S_CreateStructures() {
  Scenario.init(this, 'com.almworks.dyoma.alminternal.performance.scenarios.CreateStructuresScenario');
  this.singleUser = true;
  this.issueCount = 2000;
}

/**
 * @typedef {{size: number, get(number): T, forEach: function(T)}} List.<T>
 * @typedef {{mean: number, SD: number}} MeanAndDispersion
 */
/**
 * @class
 */
function Duration() {}
/**
 * @function
 * @return {number}
 */
Duration.prototype.toMillis = function () {};
/**
 * @class
 * @property {DomLog} log
 */
function ReportEnvironment() {}
/**
 * @function
 * @param {number[]} values
 * @return {DiscreetValue}
 */
ReportEnvironment.prototype.discreetValue = function (values) {};
/**
 * @function
 * @param {boolean} success
 * @param {string} description
 * @param {string} detailsJson
 */
ReportEnvironment.prototype.append = function (success, description, detailsJson) {};

/**
 * @class
 * @property {MeanAndDispersion} meanAndDispersion
 */
function DiscreetValue() {}
/**
 * @function
 * @param {number} ratio
 * @return {DiscreetValue}
 */
DiscreetValue.prototype.lowest = function (ratio) {};
/**
 * @return {boolean}
 */
DiscreetValue.prototype.hasData = function () {};

/**
 * @class
 * @property {List.<DomLogLine>} lines
 * @property {List.<string>} leafActivities
 */
function DomLog() {}
/**
 * @param {string} activity
 * @return {List.<LoggedRequest>}
 */
DomLog.prototype.activityRequests = function (activity) {};

/**
 * @class
 * @property {string} name
 * @property {List.<LogDomActivity>} activities
 * @property {boolean} success
 */
function DomLogLine() {}

/**
 * @class LogDomActivity
 * @property {string} name
 */
function LogDomActivity() {}

/**
 * @class
 * @property {Duration} totalDuration
 */
function LoggedRequest() {}
