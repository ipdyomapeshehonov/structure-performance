#!/usr/bin/env bash

mvn clean package -P build.app
java -Dperf.configJS=$1 -jar target/tester.jar