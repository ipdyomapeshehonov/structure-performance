# README #

### Start Dev ###

1. Create root directory and `cd` here.
2. Run:
```sh
git clone git@bitbucket.org/ipdyomapeshehonov/performance.git
git clone git@bitbucket.org/ipdyomapeshehonov/structure-performance.git
git clone ssh://git@bitbucket.org/ipdyomapeshehonov/performanceserver.git
```
3. Create root POM
```xml
<project>
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.almworks.dyoma.alminternal</groupId>
  <artifactId>performance</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>pom</packaging>

  <modules>
    <module>performance</module>
    <module>structure-performance</module>
  </modules>
</project>
```