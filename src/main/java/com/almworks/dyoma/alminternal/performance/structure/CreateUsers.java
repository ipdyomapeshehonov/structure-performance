package com.almworks.dyoma.alminternal.performance.structure;

import com.almworks.dyoma.alminternal.performance.jira.*;
import com.almworks.dyoma.alminternal.performance.runner.TestConfig;
import com.almworks.dyoma.util.http.ServiceClient;
import org.apache.http.auth.UsernamePasswordCredentials;

import javax.script.ScriptException;
import java.io.IOException;
import java.net.*;
import java.util.function.IntFunction;

/**
 * @author dyoma
 */
public class CreateUsers {
  public static void main(String[] args) throws URISyntaxException, IOException, ScriptException {
    TestConfig config = TestConfig.loadConfig();
    URI jiraUri = config.jira().getJiraUrl();
    try (ServiceClient client = ServiceClient.create(jiraUri)) {
      JiraFacade jira = new JiraFacade(client);
      UsernamePasswordCredentials adminCredentials = config.jira().getAdminCredentials();
      jira.login(adminCredentials);
      jira.webSudo(adminCredentials.getPassword());
      IntFunction<UsernamePasswordCredentials> userCredentials = config.jira().getUserCredentials();
      for (int i = 0; i < 1000; i++) {
        UsernamePasswordCredentials credentials = userCredentials.apply(i);
        JiraUser user = JiraUser.newUser(client, String.format("testuser%s@aa.bb", i), String.format("Test User #%s", i),
          credentials.getUserName(), credentials.getPassword());
        if (user == null) {
          System.err.println("failed to create user: " + credentials.getUserName());
          continue;
        }
        // Check that user has been successfully created
        try (ServiceClient test = ServiceClient.create(jiraUri)) {
          try {
            new JiraFacade(test).login(user.getUserId(), user.getPassword());
            System.out.println("User created: " + user + " " + user.getPassword());
          } catch (IOException e) {
            System.err.println("Failed to login: " + user);
            user.delete(client);
          }
        }
      }
    }
  }
}
