package com.almworks.dyoma.alminternal.performance.structure.release35;

import com.almworks.dyoma.alminternal.performance.runner.TestConfig;
import com.almworks.dyoma.alminternal.performance.server.rest.data.TestLog;
import com.almworks.dyoma.alminternal.performance.util.ScenarioHelper;
import com.almworks.dyoma.util.http.ServiceClient;
import org.apache.http.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import javax.script.ScriptException;
import java.io.*;
import java.net.*;

/**
 * @author dyoma
 */
public class UploadTestResult {
  public static void main(String[] args) throws ScriptException, IOException, URISyntaxException {
    uploadOutputZip(TestConfig.loadConfig());
  }

  public static boolean uploadOutputZip(TestConfig config) throws URISyntaxException, IOException {
    URI perfUri = config.getUploadTo();
    File outputZip = ScenarioHelper.getOutputZip(config);
    boolean isFile = outputZip.isFile();
    if (perfUri == null || !isFile) {
      if (perfUri == null) System.out.println("Upload not requested");
      if (!isFile) System.out.println("Nothing to upload: " + outputZip);
      return false;
    }
    System.out.println("Uploading file: " + outputZip);
    HttpEntity entity = MultipartEntityBuilder.create()
      .addBinaryBody(TestLog.UPLOAD_BODY_PART, outputZip)
      .build();
    try (ServiceClient client = ServiceClient.create(perfUri)) {
      TestLog log = client.post("rest/api/v1/testlogs/")
        .addHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.getMimeType()) // May be useful if the loader does not set the header
        .setEntity(entity)
        .execute(TestLog.READER);
      System.out.println("TestLog uploaded: " + log.getSelf());
      return true;
    }
  }

}
