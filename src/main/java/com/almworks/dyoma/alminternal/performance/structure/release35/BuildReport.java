package com.almworks.dyoma.alminternal.performance.structure.release35;

import com.almworks.dyoma.alminternal.performance.reports.CardReport;
import com.almworks.dyoma.alminternal.performance.reports.gatling.GatlingReport;
import com.almworks.dyoma.alminternal.performance.runner.TestConfig;
import com.almworks.dyoma.alminternal.performance.scenario.logparser.LogDom;
import com.almworks.dyoma.alminternal.performance.util.*;

import javax.script.ScriptException;
import java.io.*;

/**
 * @author dyoma
 */
public class BuildReport {
  private static final File GATLING_SH = new File("/home/dyoma/javaTools/gatling-charts-highcharts-bundle-2.2.3/bin/gatling.sh");

  public static void main(String[] args) throws ScriptException, IOException {
    TestConfig config = TestConfig.loadConfig();
    perform(config);
  }

  public static void perform(TestConfig config) throws IOException {
    File reportRoot = new File(config.getOutputDir(), "report").getAbsoluteFile();
    FileUtil.emptyDirectory(reportRoot);
    File logFile = ScenarioHelper.getLogFile(config);
    File gatlingDir = new File(reportRoot, "gatling");
    new GatlingReport()
      .setOutputDir(gatlingDir)
      .setLog(logFile)
      .setGatlingSh(GATLING_SH)
      .generateAll();
    new CardReport()
      .setRoot(reportRoot)
      .addScenario(new CardReport.Scenario() {
        @Override
        public File getLogFile() {
          return logFile;
        }

        @Override
        public void appendSummary(OutputStreamWriter writer, LogDom log) throws IOException {

        }

        @Override
        public File getGaltingIndex() {
          return new File(gatlingDir, "index.html");
        }
      })
      .buildContent();
  }
}
