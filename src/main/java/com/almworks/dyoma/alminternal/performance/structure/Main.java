package com.almworks.dyoma.alminternal.performance.structure;

import com.almworks.dyoma.alminternal.performance.*;
import com.almworks.dyoma.alminternal.performance.reports.BulkBuilder;
import com.almworks.dyoma.alminternal.performance.runner.TestConfig;
import com.almworks.dyoma.alminternal.performance.util.jsconfig.JsValue;
import com.almworks.dyoma.webtests.ServerParams;
import com.almworks.dyoma.webtests.context.Context;
import org.apache.http.auth.UsernamePasswordCredentials;

import javax.script.ScriptException;
import java.io.*;
import java.net.URISyntaxException;
import java.util.function.IntFunction;

/**
 * @author dyoma
 */
public class Main {
  public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException, ScriptException {
    TestConfig config = TestConfig.loadConfig();

    Context.Simple context = new Context.Simple();
    context.putValue(ServerParams.SERVER_URI, config.jira().getJiraUrl());
    context.putValue(PerformanceUtils.ADMIN_CREDENTIALS, config.jira().getAdminCredentials());
    File pluginJar = config.getPluginJar();
    BulkBuilder builder = new BulkBuilder(config.getOutputDir(), context);
    IntFunction<UsernamePasswordCredentials> userCredentials = config.jira().getUserCredentials();
    JsValue loadParams = config.getParameter("scenarioParams");
    config.printConfig();
    long structureId = loadParams.getLong("structureId");
    System.out.println("StructureID: " + structureId);
    LoadAndPollScenario loadForest = LoadAndPollScenario.withDefaults()
      .setStructureId(structureId)
      .setUserCredentials(userCredentials)
      .setTitle("Load Forest")
      .setPollCount(loadParams.get("poll").getInt("count"))
      .setPollPause(loadParams.get("poll").getInt("pauseSec"))
      .setUserCount(loadParams.getInt("userCount"))
      .setRampPercents(loadParams.getInt("rampPercent"))
      .setThreadCount(loadParams.getInt("threadCount"))
      .setPluginFile(pluginJar);
//    investigateConcurrencyImpact(builder, loadForest);
    testCaches(builder, loadForest);
    builder.runScenarios();
    if (config.isCreateReport()) {
      builder.buildReports();
      builder.buildContent();
    }
  }

  private static void investigateConcurrencyImpact(BulkBuilder builder, LoadAndPollScenario template) {
    initCaches(builder, template);
    int[] threadCount = {1, 3, 4, 5, 6, 7, 8, 10, 12, 15, 20, 25, 30, 35, 40};
    for (int cnt : threadCount) {
      builder.add(template.clone().setPluginFile(null).setThreadCount(cnt)
        .setSubTitle(cnt + " Thread")
        .setDescription("Collect performance of subsequent forest load at various concurrency"));
    }
  }

  private static void initCaches(BulkBuilder builder, LoadAndPollScenario template) {
    builder
      .add(template.clone().setUserCount(1)
        .setSubTitle("Reset")
        .setScenarioSubId("reset")
        .setDescription("Install plugin and init global cache"))
      .add(template.clone().setPluginFile(null).setRampPercents(0)
        .setScenarioSubId("userCaches")
        .setSubTitle("Build user caches")
        .setDescription("Initialize caches for all users"));
  }

  private static void testCaches(BulkBuilder builder, LoadAndPollScenario template) {
    template = template.clone().setScenarioSubId("testCaches");
    initCaches(builder, template);
    builder.add(template.clone().setPluginFile(null)
      .setScenarioSubId("consequent")
      .setSubTitle("Consequent")
      .setDescription("Subsequent load forest when caches are built"));
  }
}
