package com.almworks.dyoma.alminternal.performance.structure.release35;

import com.almworks.dyoma.alminternal.performance.PerformanceUtils;
import com.almworks.dyoma.alminternal.performance.runner.*;
import com.almworks.dyoma.alminternal.performance.scenarios.CreateStructuresScenario;
import com.almworks.dyoma.alminternal.performance.util.*;
import com.almworks.dyoma.webtests.ServerParams;
import com.almworks.dyoma.webtests.context.Context;

import javax.script.ScriptException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.concurrent.TimeoutException;

/**
 * @author dyoma
 */
public class CreateStructures {
  public static void main(String[] args) throws ScriptException, IOException, URISyntaxException, TimeoutException, InterruptedException {
    TestConfig config = TestConfig.loadConfig();
    FileUtil.emptyDirectory(config.getOutputDir());

    Context.Simple context = new Context.Simple();
    context.putValue(ServerParams.SERVER_URI, config.jira().getJiraUrl());
    context.putValue(PerformanceUtils.ADMIN_CREDENTIALS, config.jira().getAdminCredentials());

    try (ScenarioHelper helper = new ScenarioHelper(config)) {
      new TestStructureScenario(new Context.Child(context), helper::logMessage)
        .setPluginFile(config.getPluginJar())
        .setThreadCount(1)
        .runScenario(WarmUpStructurePlugin::createWarmUp);
      CreateStructuresScenario params = CreateStructuresScenario.create(config);
      params.beforeStart(context);
      helper.logMessage(params.getScenarioDescription());
      helper.beforeStart();
      new TestStructureScenario(new Context.Child(context), helper::logMessage)
        .setPluginFile(null)
        .setThreadCount(params.getThreadCount())
        .setProgressListener(new ConsoleScenarioProgress(Duration.ofSeconds(5)))
        .runScenario(params::createScenario);
      helper.afterFinish();
    }
    if (!UploadTestResult.uploadOutputZip(config))
      BuildReport.perform(config);
  }
}
