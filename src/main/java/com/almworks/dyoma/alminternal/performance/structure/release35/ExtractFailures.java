package com.almworks.dyoma.alminternal.performance.structure.release35;

import com.almworks.dyoma.alminternal.performance.logs.*;
import com.almworks.dyoma.alminternal.performance.logs.http.*;
import com.almworks.dyoma.util.gson.GsonUtils;
import com.almworks.dyoma.webtests.structure.rest.RestUpdateResponse;
import org.apache.http.RequestLine;

import javax.script.ScriptException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author dyoma
 */
public class ExtractFailures {
  public static void main(String[] args) throws ScriptException, IOException {
//    TestConfig config = TestConfig.loadConfig();
    File logFile = new File("/home/dyoma/Work/Projects/ALMInternal/Result/5/out.json");
//    printFailures(logFile);
//    checkConsistency(logFile);
    inspect(logFile);
  }

  private static void inspect(File logFile) throws IOException {
    MessageVisitor.CollectRequests requests = new MessageVisitor.CollectRequests();
    AtomicInteger c = new AtomicInteger();
    processLog(logFile, LogParser.Tracker.untilFirst(requests, new MessageVisitor() {
      @Override
      public boolean acceptResponse(ResponseMessage response) {
        RequestMessage req = requests.getRequest(response.getId());
        Objects.requireNonNull(req);
        RequestLine line = req.getRequestLine();
        if ("POST".equals(line.getMethod()) && "/rest/structure/2.0/forest/update".equals(line.getUri())) {
          c.incrementAndGet();
          String body = new String(response.getBodyBytes(), StandardCharsets.UTF_8);
          RestUpdateResponse update = GsonUtils.NO_EXPOSE.fromJson(body, RestUpdateResponse.class);
          if (update.forestUpdates == null)
            System.out.println(String.format("Culprit: request#%s. Response body: %s", req.getId(), body));
        }
        return true;
      }
    }));
    System.out.println(requests.getRequests().size());
    System.out.println(c.get());
  }

  private static void processLog(File logFile, LogParser.Tracker tracker) throws IOException {
    new LogParser()
      .setStrictCheck(true)
      .setTracker(tracker)
      .parseFile(logFile);
  }
}
