package com.almworks.dyoma.alminternal.performance.structure.release35;

import com.almworks.dyoma.alminternal.performance.*;
import com.almworks.dyoma.alminternal.performance.jira.*;
import com.almworks.dyoma.alminternal.performance.scenario.*;
import com.almworks.dyoma.webtests.HttpParams;
import com.almworks.dyoma.webtests.context.Context;

import java.util.*;


/**
 * @author dyoma
 */
public class WarmUpStructurePlugin {
  public static List<StoryLine> createWarmUp(Context scenarioContext) {
    Context.Child context = new Context.Child(scenarioContext);
    context.putValue(HttpParams.HTTP_CLIENT, context.getMandatory(HttpParams.HTTP_CLIENT)); // Fix client instance

    Step login = HttpStep.logIntoJira("warmUp/login", PerformanceUtils.ADMIN_CREDENTIALS);
    Step listAllStructures = HttpStep.http("warmUp/listStructures", (client, session) -> {
      List<Structure> list = Structure.list(client);
      long id = list.get(list.size() / 2).id;
      session.putValue(HttpStep.STRUCTURE, new StructureValues(new LifeStructure(client, id)));
    });
    Step loadStructure = HttpStep.structure("warmUp/load", (structure, session) -> structure.getStructure().loadRows());
    return Collections.singletonList(new StoryLine.Impl("WarmUp plugin", context,
      Arrays.asList(login, listAllStructures, loadStructure)));
  }
}
