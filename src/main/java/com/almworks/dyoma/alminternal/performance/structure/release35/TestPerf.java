package com.almworks.dyoma.alminternal.performance.structure.release35;

import com.almworks.dyoma.alminternal.performance.scriptrunner.JsScriptRunner;

import javax.script.ScriptException;
import java.io.IOException;

/**
 * @author dyoma
 */
public class TestPerf {
  public static void main(String[] args) throws ScriptException, IOException {
    System.setProperty("perf.configJS", "structure-performance/scripts/testScript.js");
    System.setProperty("perf.str.environment.outputDir", "structure-performance/out");
    System.setProperty("perf.str.PLUGIN_JAR", "/home/dyoma/javaTools/Tomcat/perfServer/perfServer/fileServer/34.data");
    JsScriptRunner.main(new String[]{"-test"}); // Run test
//    JsScriptRunner.main(new String[]{"-analyseLog", "structure-performance/out/out.zip", "-o", "structure-performance/out/out.json"}); // Build report
  }
}
