package com.almworks.dyoma.alminternal.performance.structure;

import com.almworks.dyoma.alminternal.performance.LoadAndPollScenario;
import com.almworks.dyoma.alminternal.performance.logs.*;
import com.almworks.dyoma.alminternal.performance.logs.stats.*;
import com.almworks.dyoma.alminternal.performance.reports.BulkBuilder;
import com.almworks.dyoma.alminternal.performance.runner.TestConfig;
import com.almworks.dyoma.alminternal.performance.scenario.ScenarioFailure;
import com.almworks.dyoma.alminternal.performance.scenario.logparser.*;
import com.almworks.dyoma.alminternal.performance.util.jsconfig.JsValue;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.script.ScriptException;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author dyoma
 */
@RunWith(Parameterized.class)
public class ProcessLogs {
  @Parameterized.Parameter(0)
  public File myLogFile;
  @Parameterized.Parameter(1)
  public String myTitle;
  @Parameterized.Parameter(2)
  public String myScenarioId;

  public static TestConfig ourConfig;

  @Parameterized.Parameters(name = "{1} (ID: {2})")
  public static List<Object[]> collectLogs() throws ScriptException, IOException {
    ourConfig = TestConfig.loadConfig();
    List<File> files = BulkBuilder.listLogFiles(ourConfig.getOutputDir());
    return files.parallelStream()
      .map(file -> {
        final LoadAndPollScenario[] scenario = {null};
        try {
          new LogParser()
            .setTracker((type, data) -> {
              LogMessage.Meta meta = LogMessage.Meta.fromMessage(type, data);
              LoadAndPollScenario s = LoadAndPollScenario.fromMeta(meta);
              if (s == null) return true;
              scenario[0] = s;
              return false;
            })
            .parseFile(file);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        return new Object[]{file, scenario[0].getTitle(), scenario[0].getScenarioId()};
      }).collect(Collectors.toList());
  }

  private ParsedLog myParsedLog;

  @Before
  public void prepare() throws IOException {
    myParsedLog = parsedLog(myLogFile);
  }

  private static ParsedLog ourLastLog = null;
  public static ParsedLog parsedLog(File file) throws IOException {
    if (ourLastLog == null || !file.equals(ourLastLog.mySource)) {
      ourLastLog = null;
      LogDom log = new LogDomParser().parseFile(file);
      ourLastLog = new ParsedLog(file, log);
    }
    return ourLastLog;
  }

  @AfterClass
  public static void clear() {
    ourLastLog = null;
  }

  /**
   * Looks for failures. Fails if test log contains any failure
   */
  @Test
  public void failures() {
    LogDom log = myParsedLog.myLog;
    int counter = 0;
    for (LoggedRequest request : log.getRequests().values()) {
      String message = request.getFailureMessage();
      String stackTrace = request.getStackTrace();
      if (message != null || stackTrace != null) {
        counter++;
        if (message != null) System.err.println(String.format("%nFailure: %s%n%s", message, stackTrace));
      } else {
        List<ScenarioFailure> scenarioFailures = request.getScenarioFailures();
        for (ScenarioFailure failure : scenarioFailures) {
          counter++;
          System.err.println(String.format("%nScenario failure: %s%n%s", failure.getMessage(), failure.getTrace()));
        }
      }
    }
    for (ScenarioFailure failure : log.getFailures()) {
      counter++;
      System.err.println(String.format("%nScenario failure: %s%n%s", failure.getMessage(), failure.getTrace()));
    }
    Assert.assertEquals("Failures found", 0, counter);
  }

  /**
   * Calculates statistics for request time and compares with expected duration.
   * Fails if actual time is out of expected range or no range is specified for an activity.
   */
  @Test
  public void time() {
    LogDom log = myParsedLog.myLog;
    LoadAndPollScenario scenario = myParsedLog.myScenario;
    JsValue scenarioTimings;
    try {
      scenarioTimings = ourConfig.getParameter("expectedTiming").getByPath(scenario.getScenarioId());
      if (scenarioTimings.isNull()) scenarioTimings = null;
    } catch (Exception e) {
      e.printStackTrace();
      scenarioTimings = null;
    }
    Assert.assertNotNull("Missing expected timings for: " + scenario.getScenarioId(), scenarioTimings);
    Set<String> activities = log.getLeafActivities();
    List<String> missingActivityTimings = new ArrayList<>();
    List<String> outOfRange=  new ArrayList<>();
    for (String actId : activities) {
      JsValue activityTimings = scenarioTimings.get(actId);
      if (activityTimings.isNull()) {
        missingActivityTimings.add(actId);
        continue;
      }
      if (activityTimings.getBoolean("ignore")) continue;
      try {
        double min = activityTimings.getDouble("min");
        double max = activityTimings.getDouble("max");
        List<LoggedRequest> actLog = log.selectLeafActivityId(actId);
        DiscreetValue total = DiscreetValue.create(actLog.stream().map(request -> request.getTotalDuration().toMillis()));
        StatUtils.MeanAndDispersion md = total.lowest(0.9).getMeanAndDispersion();
        if (max < md.getMean()) outOfRange.add(String.format("%s: mean (%s) > max (%s). SD=%s", actId, md.getMean(), max, md.getSD()));
        else if (min > md.getMean()) outOfRange.add(String.format("%s: mean (%s) < min (%s). SD=%s", actId, md.getMean(), min, md.getSD()));
      } catch (Exception e) {
        e.printStackTrace();
        missingActivityTimings.add(actId);
      }
    }
    if (!missingActivityTimings.isEmpty())
      System.err.println(String.format("%nMissing expected activity timings: %s", missingActivityTimings));
    if (!outOfRange.isEmpty()) {
      System.err.println(String.format("%nOutOfRange:%n"));
      for (String failure : outOfRange) System.err.println(String.format("%s%n", failure));
    }
    Assert.assertEquals("Response time out of range", 0, outOfRange.size());
    Assert.assertEquals("Missing expected activity timings", 0, missingActivityTimings.size());
  }

  private static class ParsedLog {
    private final File mySource;
    private final LogDom myLog;
    private final LoadAndPollScenario myScenario;

    public ParsedLog(File source, LogDom log) {
      mySource = source;
      myLog = log;
      myScenario = LoadAndPollScenario.fromLogInfo(log);
    }
  }
}
